package com.eightbitshort.collaboration.model;

import java.io.Serializable;
import java.util.Date;

public class Document implements Serializable {

    private static final long serialVersionUID = -7255571045444260971L;

    private Date lastUpdated;
    private String key;
    private String text;

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
