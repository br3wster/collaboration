package com.eightbitshort.collaboration.model;

import java.io.Serializable;

public class DocumentMessage implements Serializable {

    private static final long serialVersionUID = 7475380992502191837L;

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
