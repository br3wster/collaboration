package com.eightbitshort.collaboration.controller;

import com.eightbitshort.collaboration.model.Document;
import com.eightbitshort.collaboration.model.DocumentMessage;
import com.eightbitshort.collaboration.service.CollaborationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.Date;

@Controller
public class CollaborationSocketController {

    @Autowired
    @Lazy
    CollaborationService collaborationService;

    @MessageMapping("/updateDocument/{key}")
    @SendTo("/document/{key}")
    public DocumentMessage updateDocument(DocumentMessage documentMessage, @DestinationVariable String key) {
        Document document = collaborationService.getDocument(key);
        DocumentMessage response = new DocumentMessage();

        if (StringUtils.isNotBlank(documentMessage.getText())) {
            document.setText(documentMessage.getText());
            document.setLastUpdated(new Date());
        }

        response.setText(document.getText());

        return response;
    }

}
