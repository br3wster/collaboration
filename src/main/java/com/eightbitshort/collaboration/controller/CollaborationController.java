package com.eightbitshort.collaboration.controller;

import com.eightbitshort.collaboration.model.CollaborationUser;
import com.eightbitshort.collaboration.model.Document;
import com.eightbitshort.collaboration.service.CollaborationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Scope("session")
public class CollaborationController {

    @Autowired
    @Lazy
    CollaborationService collaborationService;

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/collaborate")
    public String collaborate(Model model) {
        CollaborationUser user = (CollaborationUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Document document = collaborationService.getDocument(user.getPassword());

        model.addAttribute("document", user.getPassword());
        model.addAttribute("text", document.getText());

        return "collaborate";
    }
}
