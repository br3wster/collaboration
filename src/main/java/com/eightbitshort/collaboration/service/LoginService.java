package com.eightbitshort.collaboration.service;

import com.eightbitshort.collaboration.model.CollaborationUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

@Service
public class LoginService {

    @Autowired
    private SessionRegistry sessionRegistry;

    public CollaborationUser login(String username, String key) {
        CollaborationUser user = null;

        // Check to see if we have anyone logged in with this username
        List<Object> principals = sessionRegistry.getAllPrincipals();
        HashSet<String> usernames = new HashSet<>();

        for (Object principal : principals) {
            if (principal instanceof CollaborationUser) {
                usernames.add(((CollaborationUser) principal).getUsername().toUpperCase());
            }
        }

        if (!usernames.contains(username.toUpperCase())) {
            user = new CollaborationUser();
            user.setUsername(username);
            user.setPassword(key);
        }

        return user;
    }

}
