package com.eightbitshort.collaboration.service;

import com.eightbitshort.collaboration.model.Document;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CollaborationService {

    @Cacheable("document")
    public Document getDocument(String key) {
        Document document = new Document();
        document.setKey(key);
        document.setLastUpdated(new Date());
        document.setText("");

        return document;
    }

}
