package com.eightbitshort.collaboration.config;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.StreamSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class HazelcastEncryptionSerializer implements StreamSerializer<Object> {

	private static final Logger logger = LoggerFactory.getLogger(HazelcastEncryptionSerializer.class);

	private static final byte[] key = "d!M'r4V6=|;5RzQz".getBytes();
	private static final String transformation = "AES/ECB/PKCS5Padding";

	public static void encrypt(Object object, OutputStream ostream) {
		if (object instanceof Serializable) {
			try {
				// Length is 16 byte
				SecretKeySpec sks = new SecretKeySpec(key, "AES");

				// Create cipher
				Cipher cipher = Cipher.getInstance(transformation);
				cipher.init(Cipher.ENCRYPT_MODE, sks);
				SealedObject sealedObject = new SealedObject((Serializable) object, cipher);

				// Wrap the output stream
				CipherOutputStream cos = new CipherOutputStream(ostream, cipher);
				ObjectOutputStream outputStream = new ObjectOutputStream(cos);
				outputStream.writeObject(sealedObject);
				outputStream.close();
			} catch (IllegalBlockSizeException | IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
				logger.error("Error encrypting object:", e);
			}
		}
		else {
			logger.error("Tried to serialize a non serializable object!");
		}
	}

	public static Object decrypt(InputStream istream) {
		try {
			SecretKeySpec sks = new SecretKeySpec(key, "AES");

			Cipher cipher = Cipher.getInstance(transformation);
			cipher.init(Cipher.DECRYPT_MODE, sks);

			CipherInputStream cipherInputStream = new CipherInputStream(istream, cipher);
			ObjectInputStream inputStream = new ObjectInputStream(cipherInputStream);
			SealedObject sealedObject;
			sealedObject = (SealedObject) inputStream.readObject();
			return sealedObject.getObject(cipher);
		} catch (ClassNotFoundException | IllegalBlockSizeException | BadPaddingException | IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
			logger.error("Could not decrypt object:", e);
			return null;
		}
	}

	@Override
	public void write(ObjectDataOutput out, Object object) throws IOException {
		// Encrypt here
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HazelcastEncryptionSerializer.encrypt(object, baos);
		baos.close();
		out.write(baos.toByteArray());
	}

	@Override
	public Object read(ObjectDataInput in) throws IOException {
		// Decrypt here
		final InputStream inputStream = (InputStream) in;
		return HazelcastEncryptionSerializer.decrypt(inputStream);
	}

	@Override
	public int getTypeId() {
		return 9001;
	}

	@Override
	public void destroy() {

	}
}
