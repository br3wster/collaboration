package com.eightbitshort.collaboration.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.SerializerConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class HazelcastConfiguration {

	@Bean
	public Config hazelcastConfig() {
		SerializerConfig sc = new SerializerConfig().setImplementation(new HazelcastEncryptionSerializer()).setTypeClass(Object.class);

		Config config = new Config();
		config.setProperty("hazelcast.health.monitoring.level", "OFF");
		config.getSerializationConfig().addSerializerConfig(sc);
		NetworkConfig network = config.getNetworkConfig();
		JoinConfig join = network.getJoin();

		network.setPort(5701).setPortCount(1);
		network.setPortAutoIncrement(false);
		join.getMulticastConfig().setMulticastGroup("224.42.0.69"); // Nice

		return config;
	}

}
