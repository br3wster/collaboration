package com.eightbitshort.collaboration.auth;

import com.eightbitshort.collaboration.service.LoginService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class CollaborationAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    LoginService loginService;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        // I don't think we need to do anything here
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        if (authentication == null || StringUtils.isBlank(authentication.getCredentials().toString())) {
            throw new CollaborationAuthenticationException("You must enter a document key!");
        }

        UserDetails user = loginService.login(username, authentication.getCredentials().toString());

        if (user == null) {
            throw new CollaborationAuthenticationException("Someone else is already using that name!");
        }

        return user;
    }
}
