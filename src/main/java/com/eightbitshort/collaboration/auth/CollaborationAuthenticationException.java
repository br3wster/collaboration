package com.eightbitshort.collaboration.auth;

import org.springframework.security.core.AuthenticationException;

public class CollaborationAuthenticationException extends AuthenticationException {
    public CollaborationAuthenticationException(String msg) {
        super(msg);
    }
}
