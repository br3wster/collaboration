FROM alpine:latest
RUN apk add --no-cache openjdk8
COPY java_security/* /usr/lib/jvm/java-1.8-openjdk/jre/lib/security/
COPY ROOT.jar /opt
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "/opt/ROOT.jar"]
EXPOSE 9090